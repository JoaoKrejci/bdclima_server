const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const router = require('./API/router')
const port = 3000; //porta padrão

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/', router);
app.listen(port)