var ano = process.argv[2]
var mes = process.argv[3]
var dia = process.argv[4]
var period = process.argv[5]

// 00:00, 12:00, 18:00

if (!dia || !period) {
    console.log("Parametros incorretos")
    process.exit(0)
}

var DatabaseAccess = require('./dao')
var dao = new DatabaseAccess()

function leituraMensalRandomData() {
    return [
        Math.random().toFixed(2) * 20,  //TemperaturaBulboSeco
        Math.random().toFixed(2) * 30,  //TemperaturaBulboUmido
        Math.random().toFixed(2) * 40,  //TemperaturaMaxima
        Math.random().toFixed(2) * 20,  //TemperaturaMinima
        Math.random().toFixed(2) * 30,  //SensacaoTermica
        Math.random().toFixed(2) * 100, //UmidadeRelativa
        Math.random().toFixed(2) * 100, //UmidadeRelativaMaxima
        Math.random().toFixed(2) * 100, //UmidadeRelativaMinima
        Math.random().toFixed(2) * 30,  //VelocidadeMediaVento
        ["N", "NE", "L", "SE", "S", "SO", "O", "NO"][Math.floor(Math.random() * 9)],  //DirecaoPredominanteVento
        Math.random().toFixed(2) * 3   //PressãoAtmosferica
    ]
}

function getHora(hora) {
    return !hora
        ? "00:00"
        : hora == 1 ? "12:00" : "18:00"
}

dao.query("SELECT OMM FROM Estacao", null, (error, results, fields) => {
    var randomData, fano, fmes, fdia, fhora
    results.map((omm, key) => {
        fano = parseInt(ano)
        fmes = parseInt(mes)
        fdia = parseInt(dia)
        for (let m = parseInt(period); m >= 0; m--) {

            for (fhora = 0; fhora < 3; fhora++) {
                randomData = [
                    parseInt(omm['OMM']), 
                    `${fano}-${fmes}-${fdia}`, 
                    getHora(fhora)
                ].concat(leituraMensalRandomData())
                
                dao.query(
                    "INSERT INTO LeituraHoraria VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
                    randomData,
                    (error)=>{}
                )
            }            

            if (fmes >= 8 && !(fmes % 2) || fmes < 8 && fmes % 2) fdia = fdia % 31 + 1
            else if (fmes == 2) fdia = fdia % (!(fano % 4) ? 29 : 28) + 1
            else fdia = fdia % 30 + 1

            if (fdia == 1) fmes = fmes % 12 + 1
            if (fmes == 1 && fdia == 1) fano++
        }        
    })
})