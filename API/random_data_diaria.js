var ano = process.argv[2]
var mes = process.argv[3]
var dia = process.argv[4]
var period = process.argv[5]

if (!dia || !period) {
    console.log("Parametros incorretos")
    process.exit(0)
}

var DatabaseAccess = require('./dao')
var dao = new DatabaseAccess()

function leituraMensalRandomData() {
    return [
        Math.random().toFixed(2) * 20,
        Math.random().toFixed(2) * 30,
        Math.random().toFixed(2) * 40,
        Math.random().toFixed(2) * 20,
        Math.random().toFixed(2) * 4 + 12,
        Math.random().toFixed(2) * 30,
        Math.random().toFixed(2) * 20,
        Math.random().toFixed(2) * 13
    ]
}

dao.query("SELECT OMM FROM Estacao", null, (error, results, fields) => {
    var randomData, fano, fmes, fdia
    results.map((omm, key) => {
        fano = parseInt(ano)
        fmes = parseInt(mes)
        fdia = parseInt(dia)
        for (let m = parseInt(period); m >= 0; m--) {

            randomData = [parseInt(omm['OMM']), `${fano}-${fmes}-${fdia}`].concat(leituraMensalRandomData())
            dao.query(
                "INSERT INTO LeituraDiaria VALUES (?,?,?,?,?,?,?,?,?,?)",
                randomData,
                (error) => {
                }
            )
            console.log(randomData[0] + " " + randomData[1])

            if (fmes >= 8 && !(fmes % 2) || fmes < 8 && fmes % 2) fdia = fdia % 31 + 1
            else if (fmes == 2) fdia = fdia % (!(fano % 4) ? 29 : 28) + 1
            else fdia = fdia % 30 + 1

            if (fdia == 1) fmes = fmes % 12 + 1
            if (fmes == 1 && fdia == 1) fano++
        }
        if (results.size - 1 == key) console.log("Done")
    })
})