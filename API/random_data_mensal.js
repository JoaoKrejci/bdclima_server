var ano = process.argv[2]
var mes = process.argv[3]
var period = process.argv[4]

if (!ano || !mes || !period) {
    console.log("Parametros incorretos")
    process.exit(0)
}

var DatabaseAccess = require('./dao')
var dao = new DatabaseAccess()

function leituraMensalRandomData() {
    return [
        ["N", "NE", "L", "SE", "S", "SO", "O", "NO"][Math.floor(Math.random() * 9)],
        Math.random() * 20,
        Math.random() * 20,
        Math.random() * 14,
        Math.random() * 14,
        Math.random() * 14,
        Math.random() * 20 + 360,
        Math.random() * 30,
        Math.floor(Math.random() * 31),
        Math.random() * 14,
        Math.random() * 3,
        Math.random() * 3,
        Math.random() * 40,
        Math.random() * 30,
        Math.random() * 20,
        Math.random() * 13
    ]
}

dao.query("SELECT OMM FROM Estacao", null, (error, results, fields) => {    
    var randomData, fano, fmes
    results.map(omm => {
        fano = parseInt(ano),
        fmes = parseInt(mes)    
        for (let m = parseInt(period); m >= 0; m--) {

            randomData = [parseInt(omm['OMM']), fano, fmes].concat(leituraMensalRandomData())
            dao.query(
                "INSERT INTO LeituraMensal VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
                randomData,
                (queryString, results, fields) => {
                    console.log(results)
                }
            )

            fmes = fmes % 12 + 1;
            if (fmes == 1) fano++;
        }
    })
    console.log("Done")
})