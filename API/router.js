const express = require('express')
const dao = require('./dao')
const moment = require('moment')

const router = express.Router()
const query = (new dao()).query

router.get('/estacoes', (req, res) => {
    query('SELECT OMM,NomeEstacao FROM Estacao', null,
        (error, results, fields) => {
            if (error)
                res.json(error)
            else
                res.json(results)
        })
})

router.get('/estacao/:omm', (req, res) => {
    query('SELECT * FROM Estacao WHERE OMM=?',
        parseInt(req.params.omm),
        (error, results, fields) => {
            if (error)
                res.send(error)
            else
                res.send(results[0])
        })
})

router.delete('/estacao/:omm', (req, res) => {
    query('DELETE FROM Estacao WHERE OMM=?',
        parseInt(req.params.omm),
        (error, results, fields) => {
            if (error) {
                res.send(error)
            }
            else {
                res.status(200)
                res.end()
            }
        })
})

router.put('/estacao/:omm', (req, res) => {
    values = []
    values = Object.keys(req.body).map(key => {
        if (key == 'InicioOperacao' || key == 'FimOperacao') {
            return req.body[key] == null ? null : moment(req.body[key]).format('YYYY-MM-DD')
        }
        else return req.body[key]
    })
    values.push(req.params.omm)

    query('UPDATE Estacao SET OMM=?,NomeEstacao=?,Latitude=?,Longitude=?,Altitude=?,Operante=?,InicioOperacao=?,FimOperacao=?,Cidade=?,Estado=?,Regiao=? WHERE OMM=?',
        values,
        (error, results, fields) => {
            if (error) {
                res.send(error)
            }
            else {
                res.status(200)
                res.end()
            }
        })
})

router.post('/estacao', (req, res) => {
    values = []
    values = Object.keys(req.body).map(key => {
        if (key == 'InicioOperacao' || key == 'FimOperacao') {
            return req.body[key] == null ? null : moment(req.body[key]).format('YYYY-MM-DD')
        }
        else return req.body[key]
    })
    console.log(values)

    query('INSERT INTO Estacao (OMM,NomeEstacao,Latitude,Longitude,Altitude,Operante,InicioOperacao,FimOperacao,Cidade,Estado,Regiao) VALUES (?,?,?,?,?,?,?,?,?,?,?)',
        values,
        (error, results, fields) => {
            if (error) {
                console.log(error)
                res.send(error)
            }
            else {
                res.status(200)
                res.end()
            }
        })
})

router.get('/lhs', (req, res) => {
    query('SELECT Estacao_OMM as OMM,Dia,Hora FROM LeituraHoraria \
           WHERE Estacao_OMM=? AND \
           (Dia BETWEEN ? AND ?)\
           ORDER BY Dia,Hora',
        [req.query.omm, req.query.dini, req.query.dfim],
        (error, results, fields) => {
            if (error) {
                res.json(error)
            }
            else {
                res.json(results)
            }
        })
})

router.get('/lh', (req, res) => {
    query('SELECT LeituraHoraria.*, Estacao.NomeEstacao FROM LeituraHoraria, Estacao \
           WHERE Estacao_OMM=OMM AND Estacao_OMM=? AND \
           Dia=? AND Hora=?\
           ORDER BY Dia,Hora',
        [req.query.omm, req.query.dia, req.query.hora],
        (error, results, fields) => {
            if (error) {
                res.json(error)
            }
            else {
                res.json(results[0])
            }
        })
})

router.delete('/lh', (req, res) => {
    query('DELETE FROM LeituraHoraria WHERE \
           Estacao_OMM=? AND\
           Dia=? AND Hora=?',
        [req.query.omm, req.query.dia, req.query.hora],
        (error, results, fields) => {
            if (error)
                res.send(error)
            else
                res.send(results)
        })
})

router.put('/lh', (req, res) => {
    values = []
    values = Object.keys(req.body).map(key => {
        if (key == 'Dia') {
            return req.body[key] == null ? null : moment(req.body[key]).format('YYYY-MM-DD')
        }
        else return req.body[key]
    })
    values = values.concat([req.query.omm, req.query.dia, req.query.hora])

    query('UPDATE LeituraHoraria SET \
        Estacao_OMM=?,\
        Dia=?,\
        Hora=?,\
        TemperaturaBulboSeco=?,\
        TemperaturaBulboUmido=?,\
        TemperaturaMaxima=?,\
        TemperaturaMinima=?,\
        SensacaoTermica=?,\
        UmidadeRelativa=?,\
        UmidadeRelativaMaxima=?,\
        UmidadeRelativaMinima=?,\
        VelocidadeMediaVento=?,\
        DirecaoPredominanteVento=?,\
        PressaoAtmosferica=? \
        WHERE Estacao_OMM=? AND Dia=? AND Hora=?',
        values,
        (error, results, fields) => {
            if (error) {
                res.send(error)
            }
            else {
                res.status(200)
                res.end()
            }
        })
})

router.post('/lh', (req, res) => {
    values = []
    values = Object.keys(req.body).map(key => {
        if (key == 'Dia') {
            return req.body[key] == null ? null : moment(req.body[key]).format('YYYY-MM-DD')
        }
        else return req.body[key]
    })

    query('INSERT INTO LeituraHoraria (\
        Estacao_OMM,\
        Dia,\
        Hora,\
        TemperaturaBulboSeco,\
        TemperaturaBulboUmido,\
        TemperaturaMaxima,\
        TemperaturaMinima,\
        SensacaoTermica,\
        UmidadeRelativa,\
        UmidadeRelativaMaxima,\
        UmidadeRelativaMinima,\
        VelocidadeMediaVento,\
        DirecaoPredominanteVento,\
        PressaoAtmosferica ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
        values,
        (error, results, fields) => {
            if (error) {
                console.log(error)
                res.send(error)
            }
            else {
                console.log(results)
                res.status(200)
                res.end()
            }
        })
})

router.get('/lds', (req, res) => {
    query('SELECT Estacao_OMM,Dia FROM LeituraDiaria \
           WHERE Estacao_OMM=? AND \
           (Dia BETWEEN ? AND ?)\
           ORDER BY Dia',
        [req.query.omm, req.query.dini, req.query.dfim],
        (error, results, fields) => {
            if (error)
                res.json(error)
            else
                res.json(results)
        })
})

router.get('/ld', (req, res) => {
    query('SELECT LeituraDiaria.*, Estacao.NomeEstacao FROM LeituraDiaria, Estacao \
           WHERE Estacao_OMM=OMM AND Estacao_OMM=? AND \
           Dia=?',
        [req.query.omm, req.query.dia],
        (error, results, fields) => {
            if (error) {
                res.json(error)
            }
            else {
                res.json(results[0])
            }
        })
})

router.delete('/ld', (req, res) => {
    query('DELETE FROM LeituraDiaria WHERE \
           Estacao_OMM=? AND\
           Dia=?',
        [req.query.omm, req.query.dia],
        (error, results, fields) => {
            if (error)
                res.send(error)
            else
                res.send(results)
        })
})

router.put('/ld', (req, res) => {
    values = []
    values = Object.keys(req.body).map(key => {
        if (key == 'Dia') {
            return req.body[key] == null ? null : moment(req.body[key]).format('YYYY-MM-DD')
        }
        else return req.body[key]
    })
    values = values.concat([req.query.omm, req.query.dia])

    query('UPDATE LeituraDiaria SET \
        Estacao_OMM=?, \
        Dia=?, \
        Precipitacao=?,\
        TemperaturaCompensadaMedia=?,\
        TemperaturaMaxima=?,\
        TemperaturaMinima=?,\
        Insolacao=?,\
        EvaporacaoPiche=?,\
        UmidadeRelativaMedia=?,\
        VelocidadeMediaVento=?\
        WHERE Estacao_OMM=? AND Dia=?',
        values,
        (error, results, fields) => {
            if (error) {
                console.log(error)
                res.send(error)
            }
            else {
                console.log(results)
                res.status(200)
                res.end()
            }
        })
})

router.post('/ld', (req, res) => {
    values = []
    values = Object.keys(req.body).map(key => {
        if (key == 'Dia') {
            return req.body[key] == null ? null : moment(req.body[key]).format('YYYY-MM-DD')
        }
        else return req.body[key]
    })

    query('INSERT INTO LeituraDiaria (\
        Estacao_OMM, \
        Dia, \
        Precipitacao,\
        TemperaturaCompensadaMedia,\
        TemperaturaMaxima,\
        TemperaturaMinima,\
        Insolacao,\
        EvaporacaoPiche,\
        UmidadeRelativaMedia,\
        VelocidadeMediaVento) VALUES (?,?,?,?,?,?,?,?,?,?)',
        values,
        (error, results, fields) => {
            if (error) {
                console.log(error)
                res.send(error)
            }
            else {
                console.log(results)
                res.status(200)
                res.end()
            }
        })
})

router.get('/lms', (req, res) => {
    query('SELECT Estacao_OMM,Ano,Mes FROM LeituraMensal \
           WHERE Estacao_OMM=? AND \
           ((Ano > ? AND Ano < ?) OR \
           (Ano = ? AND Mes >= ?) OR \
           (Ano = ? AND Mes <= ?))\
           ORDER BY Ano, Mes',
        [req.query.omm, req.query.aini, req.query.afim, req.query.aini, req.query.mini, req.query.afim, req.query.mfim],
        (error, results, fields) => {
            if (error)
                res.json(error)
            else
                res.json(results)
        })
})

router.get('/lm', (req, res) => {
    query('SELECT LeituraMensal.*, Estacao.NomeEstacao FROM LeituraMensal, Estacao \
           WHERE Estacao_OMM=OMM AND Estacao_OMM=? AND \
           Ano=? AND Mes=?',
        [req.query.omm, req.query.ano, req.query.mes],
        (error, results, fields) => {
            if (error) {
                res.json(error)
            }
            else {
                res.json(results[0])
            }
        })
})


router.delete('/lm', (req, res) => {
    console.log(req.query)
    query('DELETE FROM LeituraMensal WHERE \
           Estacao_OMM=? AND\
           Ano=? AND Mes=?',
        [req.query.omm, req.query.ano, req.query.mes],
        (error, results, fields) => {
            if (error)
                res.send(error)
            else
                res.send(results)
        })
})

router.put('/lm', (req, res) => {
    values = []
    values = Object.keys(req.body).map(key => req.body[key])
    values = values.concat([req.query.omm, req.query.ano, req.query.mes])

    query('UPDATE LeituraMensal SET \
        Estacao_OMM=?,\
        Ano=?,\
        Mes=?,\
        DirecaoPredominanteVento=?,\
        VelocidadeMediaVento=?,\
        VelocidadeMaximaMediaVento=?,\
        EvaporacaoPiche=?,\
        EvapotranspiracaoPotencialBH=?,\
        EvapotranspiracaoRealBH=?,\
        InsolacaoTotal=?,\
        NebulosidadeMedia=?,\
        DiasPrecipitacao=?,\
        PrecipitacaoTotal=?,\
        PressaoAtmosfericaMarMedia=?,\
        PressaoAtmosfericaMedia=?,\
        TemperaturaMaximaMedia=?,\
        TemperaturaCompensadaMedia=?,\
        TemperaturaMinimaMedia=?,\
        VisibilidadeMedia=?\
        WHERE Estacao_OMM=? AND Ano=? AND Mes=?',
        values,
        (error, results, fields) => {
            if (error) {
                res.send(error)
            }
            else {
                res.status(200)
                res.end()
            }
        })
})

router.post('/lm', (req, res) => {
    values = []
    values = Object.keys(req.body).map(key => req.body[key])

    query('INSERT INTO LeituraMensal (\
        Estacao_OMM,\
        Ano,\
        Mes,\
        DirecaoPredominanteVento,\
        VelocidadeMediaVento,\
        VelocidadeMaximaMediaVento,\
        EvaporacaoPiche,\
        EvapotranspiracaoPotencialBH,\
        EvapotranspiracaoRealBH,\
        InsolacaoTotal,\
        NebulosidadeMedia,\
        DiasPrecipitacao,\
        PrecipitacaoTotal,\
        PressaoAtmosfericaMarMedia,\
        PressaoAtmosfericaMedia,\
        TemperaturaMaximaMedia,\
        TemperaturaCompensadaMedia,\
        TemperaturaMinimaMedia,\
        VisibilidadeMedia\
        ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
        values,
        (error, results, fields) => {
            if (error) {
                console.log(error)
                res.send(error)
            }
            else {
                console.log(results)
                res.status(200)
                res.end()
            }
        })
})

module.exports = router
